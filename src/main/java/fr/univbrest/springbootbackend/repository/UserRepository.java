package fr.univbrest.springbootbackend.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import fr.univbrest.springbootbackend.entities.User;;

public interface UserRepository extends JpaRepository<User, Long> {
     
}
