package fr.univbrest.springbootbackend;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import fr.univbrest.springbootbackend.entities.User;
import fr.univbrest.springbootbackend.repository.UserRepository;

@SpringBootApplication
public class SpringbootBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootBackendApplication.class, args);
	}

	@Bean
    public CommandLineRunner commandLineRunner(UserRepository repository) {
            return args -> {
            System.out.println("Let's inspect the beans provided by Spring Boot:\n");
            repository.save(new User(9999, "fistname1", "lastname1", "email1@email.com", "M"));
			repository.save(new User(9999, "fistname2", "lastname2", "email2@email.com", "F"));
        };
    }
}
